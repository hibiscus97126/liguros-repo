# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="A Utility to loopback Pseudo-Terminals"
HOMEPAGE="https://www.ant.uni-bremen.de/whomes/rinas/nullmodem/"
SRC_URI="https://www.ant.uni-bremen.de/whomes/rinas/nullmodem/download/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
