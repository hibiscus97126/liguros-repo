# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

DESCRIPTION="Tiny system info for Unix-like operating systems"
HOMEPAGE="https://gitlab.com/jschx/ufetch"
SRC_URI="${HOMEPAGE}/-/archive/v${PV}/ufetch-v${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64 ~x86"

LICENSE="ISC"
SLOT="0"

S="${WORKDIR}/${PN}-v${PV}"

src_install() {
	exeinto /usr/bin
	newexe ufetch-gentoo ufetch
}
