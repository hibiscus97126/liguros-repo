# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit meson

DESCRIPTION="dynamic display configuration (autorandr for wayland)"
HOMEPAGE="https://git.sr.ht/~emersion/kanshi"
SRC_URI="https://git.sr.ht/~emersion/kanshi/archive/v${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64 ~arm64 ~x86"

LICENSE="MIT"
SLOT="0"
IUSE="+man"

RDEPEND="
	dev-libs/wayland
"
BDEPEND="
	${RDEPEND}
	virtual/pkgconfig
	dev-libs/wayland-protocols
"

BDEPEND+="man? ( >=app-text/scdoc-1.9.3 )"

S=${WORKDIR}/${PN}-v${PV}

src_configure() {
	local emesonargs=(
		$(meson_feature man man-pages)
	)
	meson_src_configure
}
