# Break circular dep (2019-09-12)
# (lxde-base/menu-cache-1.1.0:0/2::nokit, ebuild scheduled for merge) depends on
#  (x11-libs/libfm-1.3.1:0/5.2.1::nokit, ebuild scheduled for merge) (buildtime)
#   (lxde-base/menu-cache-1.1.0:0/2::nokit, ebuild scheduled for merge) (buildtime_slot_op)
=x11-libs/libfm-1.3.1
