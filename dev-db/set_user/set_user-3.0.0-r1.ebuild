# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7
POSTGRES_COMPAT=( 13 14 15 )

inherit postgres

MY_PV=$(ver_rs 1- _)

DESCRIPTION="PostgreSQL extension allowing privilege escalation"
HOMEPAGE="https://github.com/pgaudit/set_user"
GIT_COMMIT="6d00d258ced239f68a8273550b6aefb9eac2545f"
SRC_URI="https://github.com/pgaudit/set_user/archive/${GIT_COMMIT}.tar.gz -> ${P}-${GIT_COMMIT}.tar.gz"

LICENSE="PostgreSQL"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	>=dev-db/postgresql-9.4
"
RDEPEND="${DEPEND}"

DOCS=( CHANGELOG.md README.md RELEASENOTES.md )

S="${WORKDIR}/${PN}-${GIT_COMMIT}"

src_compile() {
	emake USE_PGXS=1 PG_CONFIG=/usr/bin/pg_config
}

src_install() {
	emake DESTDIR="${D}" install USE_PGXS=1 PG_CONFIG=/usr/bin/pg_config
}
