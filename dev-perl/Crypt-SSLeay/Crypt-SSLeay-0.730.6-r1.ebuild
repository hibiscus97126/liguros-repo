# Copyright 2021-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DIST_AUTHOR=NANIS
DIST_VERSION=0.73_06
inherit perl-module

DESCRIPTION="OpenSSL support for LWP"

LICENSE="Artistic-2"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~mips ~ppc ~ppc64 ~s390 ~sparc ~x86 ~amd64-linux ~x86-linux"
IUSE="libressl test"
RESTRICT="!test? ( test )"

RDEPEND="
	!libressl? ( >=dev-libs/openssl-0.9.7c:0= )
	libressl? ( dev-libs/libressl:0= )
	virtual/perl-MIME-Base64
"
DEPEND="
	!libressl? ( >=dev-libs/openssl-0.9.7c:0= )
	libressl? ( dev-libs/libressl:0= )
"
BDEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
	>=virtual/perl-ExtUtils-CBuilder-0.280.205
	virtual/perl-Getopt-Long
	>=dev-perl/Path-Class-0.260.0
	>=dev-perl/Try-Tiny-0.190.0
	test? (
		>=virtual/perl-Test-Simple-0.190.0
	)
"
PDEPEND="
	dev-perl/libwww-perl
	>=dev-perl/LWP-Protocol-https-6.20.0
"

PATCHES=(
	${FILESDIR}/default_dot.patch
)

src_compile() {
	mymake=(
		"OPTIMIZE=${CFLAGS}"
	)
	perl-module_src_compile
}
