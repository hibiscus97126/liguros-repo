# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit cmake

MY_PV=${PV/_beta/-beta}
DESCRIPTION="Intel(R) Open Path Guiding Library"
HOMEPAGE="https://github.com/OpenPathGuidingLibrary/openpgl"
SRC_URI="https://github.com/OpenPathGuidingLibrary/openpgl/archive/refs/tags/v${MY_PV}.tar.gz -> ${P}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	dev-cpp/tbb	
	media-libs/embree
"
DEPEND="${RDEPEND}"

S=${WORKDIR}/openpgl-${MY_PV}
