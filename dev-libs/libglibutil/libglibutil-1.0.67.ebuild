# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

DESCRIPTION="Library of glib utilities"
HOMEPAGE="https://github.com/sailfishos/libglibutil"
SRC_URI="https://github.com/sailfishos/libglibutil/archive/${PV}.tar.gz  -> ${P}.tar.gz"
KEYWORDS="~amd64"
LICENSE="BSD"
SLOT="0"

DEPEND="dev-libs/glib"
RDEPEND="${DEPEND}"
BDEPEND="virtual/pkgconfig"

src_compile() {
	emake KEEP_SYMBOLS=1
}

src_install() {
	emake DESTDIR="${D}" LIBDIR="/usr/$(get_libdir)" install-dev
}
