# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

USE_RUBY="ruby26 ruby27 ruby30"
inherit autotools flag-o-matic multilib-minimal ruby-single

DESCRIPTION="Alternative to vendor specific OpenCL ICD loaders"
HOMEPAGE="https://github.com/OCL-dev/ocl-icd"
SRC_URI="https://github.com/OCL-dev/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="khronos-headers"

BDEPEND="${RUBY_DEPS}"
DEPEND="!khronos-headers? ( =dev-util/opencl-headers-2021.04.29 )"
RDEPEND="${DEPEND}
	!app-eselect/eselect-opencl
	!dev-libs/opencl-icd-loader"

src_prepare() {
	replace-flags -Os -O2 # bug 646122

	default
	eautoreconf
}

multilib_src_configure() {
	local myconf=(
		--enable-pthread-once
		$(use_enable khronos-headers official-khronos-headers)
	)

	#ECONF_SOURCE="${S}" econf --enable-pthread-once --disable-official-khronos-headers
	ECONF_SOURCE="${S}" econf "${myconf[@]}"
}

multilib_src_install() {
	default

	# Drop .la files
	find "${ED}" -name '*.la' -delete || die
}
