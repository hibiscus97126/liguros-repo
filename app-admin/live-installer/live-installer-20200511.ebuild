# Copyright 2020-2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

COMMIT_ID="e93a28cb3eb9e15935b2dd0c11f8d265abc9d894"
DESCRIPTION="Linux Mint Live Installer"
HOMEPAGE="https://github.com/linuxmint/live-installer"
SRC_URI="${HOMEPAGE}/archive/${COMMIT_ID}.zip"

LICENSE="LGPL-2.1 LGPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
S=${WORKDIR}/${PN}-${COMMIT_ID}
RESTRICT="mirror"

RDEPEND="
	dev-python/pillow
	dev-python/pyparted
	x11-apps/setxkbmap"

src_install() {
	mv ${S}/etc "${ED}" || die
	mv ${S}/usr "${ED}" || die

	rm "${ED}"/usr/share/icons/live-installer.xpm || die
	cp -r "${ED}"/usr/share/icons "${ED}"/usr/share/pixmaps || die
}
