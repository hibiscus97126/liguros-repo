# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit meson

SRC_URI="https://gitlab.gnome.org/jadahl/libdecor/-/archive/${PV}/${P}.tar.gz"
KEYWORDS="~amd64"

DESCRIPTION="A client-side decorations library for Wayland clients"
HOMEPAGE="https://gitlab.gnome.org/jadahl/libdecor"
LICENSE="MIT"
SLOT="0"
IUSE="+dbus"

DEPEND="
	>=dev-libs/wayland-1.18
	>=dev-libs/wayland-protocols-1.15
	dbus? ( sys-apps/dbus )
	x11-libs/pango
	x11-libs/libxkbcommon
"
RDEPEND="${DEPEND}"
BDEPEND=">=dev-util/meson-0.47"
