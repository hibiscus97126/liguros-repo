# Copyright 2021 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

inherit desktop flag-o-matic toolchain-funcs xdg-utils

DESCRIPTION="Teletext viewer for X11"
HOMEPAGE="https://gitlab.com/alevt"
SRC_URI="https://gitlab.com/${PN}/${PN}/-/archive/v${PV}/${PN}-v${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~sparc x86"
RESTRICT="strip"

RDEPEND="
	x11-libs/libX11
	media-libs/libpng:="
DEPEND="${RDEPEND}
	x11-base/xorg-proto"

PATCHES=(
	"${FILESDIR}"/${P}-libpng15.patch
)

S="${WORKDIR}/${PN}-v${PV}"

src_configure() {
	append-cflags -fno-strict-aliasing
	tc-export BUILD_CC CC
}

src_install() {
	dobin alevt alevt-cap alevt-date
	doman alevt.1x alevt-date.1 alevt-cap.1
	einstalldocs

	newicon -s 16 contrib/mini-alevt.xpm alevt.xpm
	newicon -s 48 contrib/icon48x48.xpm alevt.xpm
	make_desktop_entry alevt "AleVT" alevt
}

pkg_postinst() {
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_icon_cache_update
}
