# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

inherit gnome2-utils meson xdg-utils

GIT_COMMIT="dc04e0e714df5f59fa40efffcb2b9089de8819e3"
DESCRIPTION="Matrix messaging app for GNOME written in Rust"
HOMEPAGE="https://wiki.gnome.org/Apps/Fractal"

# fractal frequently uses unreleased versions of crates
# They provide a tar with these crates vendored for released versions of fractal
RESTRICT="network-sandbox"
SRC_URI="https://gitlab.gnome.org/GNOME/${PN}/-/archive/${GIT_COMMIT}/${PN}-${GIT_COMMIT}.tar.gz -> ${PN}-${GIT_COMMIT}.tar.gz"

KEYWORDS="~amd64"
S="${WORKDIR}/${PN}-${GIT_COMMIT}"

LICENSE="GPL-3+"
SLOT="0"
IUSE="debug libressl"

RDEPEND="
	app-text/gspell
	dev-libs/glib
	libressl? ( <=dev-libs/libressl-3.4.1 )
	!libressl? ( dev-libs/openssl:0= )
	gui-libs/libhandy:1
	gui-libs/libadwaita
	gui-libs/gtksourceview
	media-libs/gst-plugins-bad
	media-libs/gst-plugins-base
	media-libs/gstreamer
	media-libs/gstreamer-editing-services
	sys-apps/dbus
	x11-libs/cairo
	x11-libs/gdk-pixbuf
	x11-libs/gtk+:3
	x11-libs/gtksourceview:4
"
DEPEND="${RDEPEND}"

src_prepare() {
	default
}

src_configure() {
	local emesonargs=(
		-Dprofile=$(usex debug 'development' 'default')
	)
	meson_src_configure
}

pkg_postinst() {
	gnome2_schemas_update
	xdg_icon_cache_update
}

pkg_postrm() {
	gnome2_schemas_update
	xdg_icon_cache_update
}
