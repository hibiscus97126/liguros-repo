# Copyright 2020-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

EGO_PN="github.com/GoogleContainerTools/${PN}/..."
S="${WORKDIR}/${P}/src/${EGO_PN%/*}"

inherit go-module git-r3

DESCRIPTION="tool for analyzing and comparing container images"
HOMEPAGE="https://github.com/GoogleContainerTools/container-diff"

EGIT_REPO_URI="https://github.com/GoogleContainerTools/container-diff.git"
EGIT_COMMIT="v0.17.0"
EGIT_CHECKOUT_DIR="${S}"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~arm"
IUSE=""

DEPEND=""

src_compile() {
	cd ${S}
	mkdir bin
	go build -v -o bin/${PN} main.go
}

src_install() {
	cd ${S}
	dobin bin/${PN}
}
