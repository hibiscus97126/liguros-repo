# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10,11} )
inherit cmake python-any-r1

DESCRIPTION="Kcov is a code coverage tester for compiled languages, Python and Bash"
HOMEPAGE="https://github.com/SimonKagstrom/kcov"
SRC_URI="https://github.com/SimonKagstrom/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
IUSE="+binutils"
KEYWORDS="~amd64 ~x86"

RDEPEND="
	dev-libs/elfutils
	net-misc/curl
	sys-libs/zlib
	binutils? ( sys-libs/binutils-libs:= )
"
DEPEND="${RDEPEND}"
BDEPEND="${PYTHON_DEPS}"

src_prepare(){
	eapply -p1 ${FILESDIR}/binutils.patch
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DCMAKE_DISABLE_FIND_PACKAGE_Bfd=$(usex !binutils)

		-DKCOV_INSTALL_DOCDIR=share/doc/${PF}
	)

	cmake_src_configure
}
