# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8
KEYWORDS="~amd64"

inherit go-module bash-completion-r1

DESCRIPTION="Detects ineffectual assignments in Go code."
HOMEPAGE="https://github.com/gordonklaus/ineffassign"
GIT_COMMIT="2e10b26642541670df2672e035b2de19fcb04cab"
SRC_URI="
	https://github.com/gordonklaus/${PN}/archive/${GIT_COMMIT}.zip -> ${P}.zip
	https://gitlab.com/liguros/distfiles/-/raw/main/${P}-r1-deps.tar.xz
"

LICENSE="MIT License"
SLOT="0"
KEYWORDS="amd64 x86 arm"
S="${WORKDIR}/${PN}-${GIT_COMMIT}"

src_compile() {
	cd ${S}
	mkdir -pv bin || die
	go build -o "${S}/bin/${PN}" || die
}

src_install() {
	dobin bin/*
	dodoc LICENSE README.md
}
