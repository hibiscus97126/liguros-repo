# Copyright 2021-2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

QT5_KDEPATCHSET_REV=1
QT5_MODULE="qtbase"
inherit qt5-build

DESCRIPTION="Network abstraction library for the Qt5 framework"

if [[ ${QT5_BUILD_TYPE} == release ]]; then
	KEYWORDS="~amd64 ~arm ~arm64 ~hppa ~ppc ~ppc64 ~riscv ~sparc ~x86"
fi

IUSE="connman dtls gssapi libressl libproxy networkmanager sctp +ssl"
REQUIRED_USE="libressl? ( !dtls )"

DEPEND="
	~dev-qt/qtcore-${PV}:5=
	sys-libs/zlib:=
	connman? ( ~dev-qt/qtdbus-${PV} )
	gssapi? ( virtual/krb5 )
	libproxy? ( net-libs/libproxy )
	networkmanager? ( ~dev-qt/qtdbus-${PV} )
	sctp? ( kernel_linux? ( net-misc/lksctp-tools ) )
	ssl? (
		!libressl? ( >=dev-libs/openssl-1.1.1:0= )
		libressl? ( >=dev-libs/libressl-3.5.0:0= )
	)
"
RDEPEND="${DEPEND}
	connman? ( net-misc/connman )
	networkmanager? ( net-misc/networkmanager )
"

QT5_TARGET_SUBDIRS=(
	src/network
	src/plugins/bearer/generic
)

QT5_GENTOO_CONFIG=(
	libproxy:libproxy:
	ssl::SSL
	ssl::OPENSSL
	ssl:openssl-linked:LINKED_OPENSSL
)

QT5_GENTOO_PRIVATE_CONFIG=(
	:network
)

PATCHES=(
	"${FILESDIR}"/${PN}-5.15.3-qsslsocket_openssl_symbols_p_h.patch
	"${FILESDIR}"/${PN}-5.15.3-qsslcontext_openssl_cpp.patch
	"${FILESDIR}"/${PN}-5.15.3-qsslsocket_openssl_symbols_cpp.patch
	"${FILESDIR}"/${PN}-5.15.3-qsslcontext_openssl_p_h.patch
	"${FILESDIR}"/${PN}-5.15.3-qsslcertificate_openssl_cpp.patch
)

pkg_setup() {
	use connman && QT5_TARGET_SUBDIRS+=(src/plugins/bearer/connman)
	use networkmanager && QT5_TARGET_SUBDIRS+=(src/plugins/bearer/networkmanager)
}

src_configure() {
	local myconf=(
		$(usev connman -dbus-linked)
		$(qt_use gssapi feature-gssapi)
		$(qt_use libproxy)
		$(usev networkmanager -dbus-linked)
		$(qt_use sctp)
		$(qt_use dtls)
		$(usev ssl -openssl-linked)
	)

	qt5-build_src_configure
}

src_install() {
	qt5-build_src_install

	# workaround for bug 652650
	if use ssl; then
		sed -e "/^#define QT_LINKED_OPENSSL/s/$/ true/" \
			-i "${D}${QT5_HEADERDIR}"/Gentoo/${PN}-qconfig.h || die
	fi
}
