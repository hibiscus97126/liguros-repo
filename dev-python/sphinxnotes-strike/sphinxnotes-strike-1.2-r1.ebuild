# Copyright 2022 Liguros Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=8

PYTHON_COMPAT=( python3_{7,8,9,10,11} )
PYTHON_REQ_USE="ncurses"

inherit distutils-r1

DESCRIPTION="Sphinx extension for strikethrough text support"
HOMEPAGE="https://sphinx.silverrainz.me/strike/"
SRC_URI="https://files.pythonhosted.org/packages/c6/13/873cb1a4e0e3501c2ed6f96317385ff5492f601a8d7b547dd30ed370cb1e/sphinxnotes-strike-1.2.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~ia64 ~mips ppc ppc64 ~sparc x86 ~amd64-linux ~x86-linux"
IUSE="examples"

DEPEND="
	dev-python/sphinx
"

distutils_enable_sphinx docs
distutils_enable_tests setup.py

